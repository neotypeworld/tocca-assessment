//
//  ViewController.swift
//  Assessment
//
//  Created by Yue Li on 7/29/19.
//  Copyright © 2019 Yue Li. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var lbutton: UIButton!
    @IBOutlet weak var rButton: UIButton!
    @IBOutlet weak var profileView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.baseView.addBottomRoundedEdge(desiredCurve: 1)
        self.cardView.roundCorned()
        self.lbutton.makeCircularButton()
        self.rButton.makeCircularButton()
        self.profileView.makeCircularImage()
        
        // Do any additional setup after loading the view.
        /*
        let shape = CAShapeLayer()
        self.view.layer.addSublayer(shape)
        shape.strokeColor = UIColor.blue.cgColor
        shape.fillColor = UIColor.blue.cgColor
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: self.view.bounds.size.width, y: 0))
        path.addLine(to: CGPoint(x: self.view.bounds.size.width, y: (3.2*self.view.bounds.height)/5))
        path.addCurve(to: CGPoint(x: 0, y: (3.2*self.view.bounds.height)/5), controlPoint1: CGPoint(x: self.view.bounds.size.width/2, y: (3.4*self.view.bounds.height)/5), controlPoint2: CGPoint(x: self.view.bounds.size.width/2, y: (3.4*self.view.bounds.height)/5))
        path.close()
        shape.path = path.cgPath
        */
    }
    
    

}

extension UIView {
    func addBottomRoundedEdge(desiredCurve: CGFloat?) {
        let offset: CGFloat = self.frame.width / desiredCurve!
        let bounds: CGRect = self.bounds
        
        let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height / 2)
        let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
        let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
        let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
        rectPath.append(ovalPath)
        
        // Create the shape layer and set its path
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = rectPath.cgPath
        
        // Set the newly created shape layer as the mask for the view's layer
        self.layer.mask = maskLayer
    }
    
    func roundCorned() {
        self.layer.cornerRadius = 10
    }
    
}

extension UIButton {
    
    func makeCircularButton() {
        self.layer.cornerRadius = self.bounds.size.width / 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        self.layer.shadowRadius = 8
        self.layer.shadowOpacity = 0.1
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.1
    }
    
}

extension UIImageView {
    
    func makeCircularImage() {
        self.layer.cornerRadius = self.bounds.size.width / 2
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        self.layer.shadowRadius = 8
        self.layer.shadowOpacity = 0.1
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.1
    }
}
